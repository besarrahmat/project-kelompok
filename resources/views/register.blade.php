@extends('layout/main')

@section('title', 'CREATE AN ACCOUNT')

@section('content')
<div class="box-register">
    <form method='post' accept-charset='UTF-8'>
        <legend>Register</legend>
        <div class="register-name">
            <input type="text" class="form-control" name="name" id="name" placeholder="Enter Name" />
        </div>
        <div class="register-email">
            <input type="text" class="form-control" name="email" id="email" placeholder="Enter Email" />
        </div>
        <div class="register-password">
            <input type="password" class="form-control" name="password" id="password" placeholder="Enter Password" />
        </div>
        <div class="confirm-password">
            <input type="password" class="form-control" name="confirm-password" id="confirmPassword" placeholder="Confirm Password" />
        </div>
        <small name="PasswordHelp" class="form-text text-muted text-center">
            Remember to make a combination of characters for your password.
        </small>
        <button type="submit" class="button-register" id="registerButton">Register</button>
    </form>
    <div id="register-to-login">
        <div class="to-login-text">Sudah punya akun?</div>
        <p class="register-to-login">
            <a href="/login" class="button-to-login">LOGIN AJA</a>
        </p>
    </div>
</div>
@endsection