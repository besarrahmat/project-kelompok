<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>@yield('title')</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/styles.css') }}" rel="stylesheet">
</head>

<body>
    @yield('content')

    <footer>
        <div class="container">
            <div class="info">
                <div class="text-center">&copy; Copyright - BESAR RAHMAT, ALVAN DWI AKBAR, 
                    KEVIN RENJIRO, DENNY MANUEL, KURNIA AMONITO - 2021</div>
            </div>
        </div>
    </footer>
</body>

</html>