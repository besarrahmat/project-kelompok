@extends('layout/main')

@section('title', 'TUGAS KELOMPOK PEMWEBLAN')

@section('content')
    <header>
        <nav id="nav" class="navbar navbar-default">
            <div class="container">
                <div class="navbar-header">
                    <a href="/">
                        <h1>Note Project</h1>
                    </a>
                </div><!-- .navbar-header -->
                <div class="navbar-login">
                    <a href="/login">Login</a>
                </div>
            </div><!-- .container -->
        </nav><!-- #nav -->
    </header>

    <div id="main" class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="content">
                    <h2>TUGAS BRO</h2>
                    <h5>by Zorion.inc</h5>
                    <p class="main-register">
                        <a href="/register" class="start-button-register">DAFTAR LAH</a>
                    </p>
                    <p class="main-login">
                        <a href="/login" class="start-button-login">SKUY MASUK</a>
                    </p>
                </div>
            </div>
        </div>
    </div>

    <div id="quote" class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="quote-start">
                    <img src="{{ asset('welcome-quote.svg') }}">
                </div>
                <div class="quote-group">
                    <blockquote class="quote-content">
                        "SEMANGAT LUR!!!"
                    </blockquote>
                    <blockquote class="quote-person">— Besar Rahmat</blockquote>
                </div>
            </div>
        </div>
    </div>
@endsection