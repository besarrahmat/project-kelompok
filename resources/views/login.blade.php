@extends('layout/main')

@section('title', 'WELCOME BACK')

@section('content')
    <div class="box-login">
        <form method='post' accept-charset='UTF-8'>
            <legend>Login</legend>
            <div class="login-email">
                <input type="text" class="form-control" name="email" id="email" placeholder="Enter Email" />
            </div>
            <div class="login-password">
                <input type="password" class="form-control" name="password" id="password" placeholder="Enter Password" />
            </div>
            <small name="EmailHelp" class="form-text text-muted text-center">
                We'll never share your password with anyone else.
            </small>
            <button type="submit" class="button-login" id="loginButton">Login</button>
        </form>
        <div id="login-to-register">
            <div class="to-register-text">Belum punya akun?</div>
            <p class="login-to-register">
                <a href="/register" class="button-to-register">DAFTAR LAH</a>
            </p>
        </div>
    </div>
@endsection
